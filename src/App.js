import { Routes, Route, Navigate } from 'react-router-dom';
import Login from './pages/Login';
import Home from './pages/Home';
import Register from './pages/Register';
import DocumentContent from './pages/documentContent';
import AdminDashboard from './pages/AdminDashboard';
import Memo from './pages/Memo';

import { useSelector } from 'react-redux';


function App() {
  const user = useSelector((state) => state.user);
  const isAdmin = user?.isAdmin;

  return (
    <Routes>
      <Route path="/" element={!user ? <Navigate to="/login" replace /> : <Home />} />
      <Route path="/login" element={user ? <Navigate to="/" replace /> : <Login />} />
      <Route path="/register" element={user ? <Navigate to="/" replace /> : <Register />} />
      <Route path="/document/:id" element={!user ? <Navigate to="/login" replace /> : <DocumentContent />} />
      <Route
        path="/dashboard"
        element={!user ? <Navigate to="/login" replace /> : (!isAdmin ? <Navigate to="/" replace /> : <AdminDashboard />)}
      />
      <Route path="/memos" element={!user ? <Navigate to="/login" replace /> : <Memo />} />
      {!user && <Route path="*" element={<Navigate to="/login" replace />} />}
    </Routes>
  );
}
export default App;
