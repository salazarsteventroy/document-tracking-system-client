import React, { useEffect, useState } from 'react';
import { Card, Navbar, Nav } from 'react-bootstrap';
import axios from "axios";
import { useNavigate } from 'react-router-dom';
import TopBar from './TopBar';
import { useLocation } from 'react-router-dom';
import { useSelector } from 'react-redux';

const Docs = () => {
  const location = useLocation();
  const [documents, setDocuments] = useState([]);
  const [filteredDocuments, setFilteredDocuments] = useState([]);
  const navigate = useNavigate();
  const [status, setStatus] = useState([]);
  const id = location.pathname.split("/")[2];
  const userId = useSelector((state) => state.user._id);
  const userRole = useSelector((state) => state.user.role);
  const isAdmin = useSelector((state) => state.user.isAdmin);


  useEffect(() => {
    const fetchDocuments = async () => {
      try {
        let response;
        if (userRole === 'standard' && !isAdmin) {
          response = await axios.get(`http://localhost:5000/api/memo/find/user/${userId}`);
        } else {
          response = await axios.get('http://localhost:5000/api/memo/');
        }
        const sortedDocuments = response.data.sort((a, b) => new Date(b.createdOn) - new Date(a.createdOn));
        setDocuments(sortedDocuments);
        setFilteredDocuments(sortedDocuments);
      } catch (error) {
        console.error(error);
      }
    };
  
    fetchDocuments();
  }, [userRole, isAdmin, userId]);
  

  const markDocumentAsOpened = async (id) => {
    try {
      await axios.put(`http://localhost:5000/api/memo/${id}`, { opened: true });
      // Optional: You can perform any additional actions after marking the document as opened.
      setDocuments((prevDocuments) =>
        prevDocuments.map((doc) =>
          doc._id === id ? { ...doc, opened: true } : doc
        )
      );
    } catch (error) {
      console.error(error);
    }
  };



  return (
    <>
      <div style={{ marginRight: "-295px", position: "relative" }}>
        <TopBar documents={documents} setFilteredDocuments={setFilteredDocuments} />
      </div>
      <div className="vh-100" style={{ minWidth: "100vh" }}>
        <Nav
          className="bg-dark text-light d-flex justify-content-around rounded"
          style={{ padding: "5px", marginBottom: "5px" }}
        >
          <Navbar.Brand
            href="#All"
            className={"mr-3 text-light"}
            onClick={() => setStatus("")}
          >
            MEMOS
          </Navbar.Brand>
        </Nav>

        <div
          style={{
            overflowY: "auto",
            maxHeight: "calc(104vh - 150px)",
          }}
        >
          {filteredDocuments.length > 0 ? (
            filteredDocuments.map((doc) => (
              <Card
                className="mx-auto border-dark bg-dark"
                style={{
                  marginBottom: "3px",
                  width: "50rem",
                  cursor: "pointer",
                  transition: "transform 0.2s ease-out",
                  minWidth: "99%",
                }}
                key={doc.id}
                onClick={() => {
                  markDocumentAsOpened(doc._id);
                  navigate(`/document/${doc.documentId}`);
                }}
                onMouseEnter={(e) => (e.target.style.transform = "scale(1.01)")}
                onMouseLeave={(e) => (e.target.style.transform = "scale(1)")}
              >
                <Card.Header>
                  <Card.Title>
                    <h6 className="outline-light text-danger mt-2">
                      {doc.title}
                    </h6>
                  </Card.Title>
                </Card.Header>
                <Card.Body className="d-flex justify-content-between">
                  <div className="d-flex flex-column">
                    <Card.Subtitle className="mb-2 text-muted">
                      {doc.documentId}
                    </Card.Subtitle>
                    <Card.Text className="text-light">
                      {doc.description}
                    </Card.Text>
                    <Card.Text className="text-success">
                      {doc.message}
                    </Card.Text>
                  </div>
                  <Card.Footer className="align-self-end">
                    <small className="text-muted">
                      Created on {doc.createdAt}
                    </small>
                  </Card.Footer>
                </Card.Body>
              </Card>
            ))
          ) : (
            <p>No documents found.</p>
          )}
        </div>
      </div>
    </>
  );
};

export default Docs;
