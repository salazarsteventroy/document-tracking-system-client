import React, { useState } from 'react';
import { Button, Form } from 'react-bootstrap';
import { useSelector } from 'react-redux';
import axios from 'axios';

const UserSettingsModal = ({ firstName, lastName, email }) => {
  const useremail = useSelector((state) => state.user.email);
  const userfirstName = useSelector((state) => state.user.firstName);
  const userlastName = useSelector((state) => state.user.lastName);
  const userId = useSelector((state) => state.user._id);

  const [oldPassword, setOldPassword] = useState('');
  const [newPassword, setNewPassword] = useState('');

  const handleUpdatePassword = async () => {
    try {
      const response = await axios.put(`http://localhost:5000/api/users/${userId}`, {
        password: newPassword,
      });

      // Handle success response
      console.log('Password updated successfully:', response.data);
    } catch (error) {
      // Handle error
      console.error('Error updating password:', error);
    }
  };

  return (
    <>
      <Form>
        <div style={{ display: 'flex' }}>
          <Form.Group className="w-50 m-1" controlId="formFirstName">
            <Form.Label>First Name</Form.Label>
            <Form.Control type="text" value={userfirstName} readOnly />
          </Form.Group>
          <Form.Group className="w-50 m-1" controlId="formLastName">
            <Form.Label>Last Name</Form.Label>
            <Form.Control type="text" value={userlastName} readOnly />
          </Form.Group>
        </div>

        <Form.Group controlId="formEmail">
          <Form.Label>Email</Form.Label>
          <Form.Control type="email" value={useremail} readOnly />
        </Form.Group>

        <p className="text-muted" style={{ marginTop: '20px' }}>
          Change Password
        </p>

        {/* <Form.Group controlId="formOldPassword">
          <Form.Label>Enter Old Password</Form.Label>
          <Form.Control
            type="password"
            value={oldPassword}
            onChange={(e) => setOldPassword(e.target.value)}
          />
        </Form.Group> */}

        <Form.Group controlId="formNewPassword">
          <Form.Label>Enter New Password</Form.Label>
          <Form.Control
            type="password"
            value={newPassword}
            onChange={(e) => setNewPassword(e.target.value)}
          />
        </Form.Group>

        <Button variant="dark" onClick={handleUpdatePassword} style={{ marginTop: '10px' }}>
          Change Password
        </Button>
      </Form>
    </>
  );
};

export default UserSettingsModal;
