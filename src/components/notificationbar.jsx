import React, { useState } from "react";
import "../css/home.scss";
import { useSelector } from "react-redux";
import axios from 'axios';
import { useEffect } from "react";

function NotificationBar() {
  const [numNotifications, setNumNotifications] = useState(0);
  const [showMessages, setShowMessages] = useState(true);
  const [documents, setDocuments] = useState([]);
  const [memos, setMemos] = useState([]);

  const userId = useSelector((state) => state.user._id);

  // useEffect(() => {
  //   const getDocuments = async () => {
  //     try {
  //       const url = `http://localhost:5000/api/documents/find/user/${userId}`;
  //       const res = await axios.get(url);
  //       const sortedDocuments = res.data.sort((a, b) => new Date(b.createdDate) - new Date(a.createdDate));
  //       const filteredDocuments = sortedDocuments.filter((doc) => !doc.opened);
  //       console.log(res);
  //       setDocuments(filteredDocuments);
  //       setNumNotifications(filteredDocuments.length);
  //     } catch (err) {
  //       console.error(err);
  //     }
  //   };
  //   getDocuments();
  // }, [userId]);


  // useEffect(() => {
  //   const getMemos = async () => {
  //     try{
  //       const url = `http://localhost:5000/api/memo/find/user/${userId}`;
  //       const response = await axios.get(url);
  //       const sortedMemos = response.data.sort((a,b) => new Date (a.createdOn) - new Date(b.createdOn));
  //       const filteredMemos = sortedMemos.filter((doc) => !doc.opened);
  //       setNumNotifications(filteredMemos.length);

  //       setMemos(filteredMemos);
  //     }catch(err){
  //       console.error(err);
  //     }
  //   };
  //   getMemos();
  // }, [userId]);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const documentUrl = `http://localhost:5000/api/documents/find/user/${userId}`;
        const memoUrl = `http://localhost:5000/api/memo/find/user/${userId}`;

        const [documentResponse, memoResponse] = await Promise.all([
          axios.get(documentUrl),
          axios.get(memoUrl),
        ]);

        const sortedDocuments = documentResponse.data.sort(
          (a, b) => new Date(b.createdDate) - new Date(a.createdDate)
        );
        const sortedMemos = memoResponse.data.sort(
          (a, b) => new Date(a.createdOn) - new Date(b.createdOn)
        );

        const filteredDocuments = sortedDocuments.filter((doc) => !doc.opened);
        const filteredMemos = sortedMemos.filter((memo) => !memo.opened);

        setDocuments(filteredDocuments);
        setMemos(filteredMemos);

        const totalNotifications = filteredDocuments.length + filteredMemos.length;
        setNumNotifications(totalNotifications);
      } catch (err) {
        console.error(err);
      }
    };

    fetchData();
  }, [userId]);


  const handleClick = (e) => {
    e.stopPropagation();
    setShowMessages(!showMessages);
  };

  const handleAlertClick = async (documentId) => {
    try {
      // Make an API call to update the document's opened field
      const documentUrl = `http://localhost:5000/api/documents/${documentId}`;
      await axios.put(documentUrl, { opened: true });
  
      // Navigate to the document's URL
      window.location.href = `http://localhost:3000/document/${documentId}`;
  
      // If memoId is provided, handle memo-related operations
    } catch (err) {
      console.error(err);
    }
  };
  
  const handleMemoAlertClick = async (memoId) => {
    try {
      // Make an API call to retrieve the memo data
      const singleMemoUrl = `http://localhost:5000/api/memo/find/${memoId}`;
      const response = await axios.get(singleMemoUrl);
      const memo = response.data;
  
      // Extract the associated document ID from the memo
      const documentInMemoId = memo.documentId;
  
      // Make an API call to update the memo's opened field
      const memoUrl = `http://localhost:5000/api/memo/${memoId}`;
      await axios.put(memoUrl, { opened: true });
  
      // Navigate to the corresponding document's URL
      window.location.href = `http://localhost:3000/document/${documentInMemoId}`;
    } catch (err) {
      console.error(err);
    }
  };
  
  


  return (
    <>
      {numNotifications > 0 ? (
        <div className="alert alert-info bg-warning" onClick={handleClick}>
          <strong>Ding!</strong> You have {numNotifications} new notifications.
        </div>
      ) : (
        <div className="alert alert-info" onClick={handleClick}>
          You have no new notifications.
        </div>
      )}

      {showMessages && (
        <div className="notificationz">
          {documents.map((doc) => (
            <div
              key={doc._id}
              className="alert alert-info"
              style={{ overflowWrap: "break-word" }}
              onClick={() => handleAlertClick(doc._id)}
            >
              <strong>Document:</strong> {doc.title}
            </div>
          ))}

              {memos.map((memo) => (
                <div
                  key={memo._id}
                  className="alert alert-info"
                  style={{ overflowWrap: "break-word" }}
                  onClick={() => handleMemoAlertClick(memo._id)}
                >
                  <strong>Memo:</strong> {memo.title}
                </div>
              ))}
        </div>
      )}
    </>
  );
}

export default NotificationBar;