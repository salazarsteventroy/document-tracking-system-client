import React, { useEffect, useState } from 'react';
import { Container, Row, Col, Card, ListGroup, Button, Form } from 'react-bootstrap';
import { BsPencilSquare } from 'react-icons/bs'; // Import the edit icon
import TopBar from './TopBar';
import Chart from 'chart.js/auto';
import '../css/dashboard.css';
import { publicRequest } from '../requestMethods';
import axios from "axios";
import { toast, ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { saveAs } from 'file-saver';




const DashboardContents = () => {
  const chartRef = React.useRef();
  const [users, setUsers] = useState([]);
  const [selectedUser, setSelectedUser] = useState(null); // Track the selected user
  const [documents, setDocuments] = useState([]);
  const [memos, setMemos] = useState([]);
  const [documentCount, setDocumentCount] = useState([]);
  const [chartInstance, setChartInstance] = useState(null);
  

  


    useEffect(() => {
      const fetchMemos = async () => {
        try {
          const res = await axios.get('http://localhost:5000/api/memo/');
          setMemos(res.data);
          console.log(res);
        } catch (error) {
          console.error('Failed to get memos', error);
        }
      };
      fetchMemos();
    }, []);
  
    useEffect(() => {
      const fetchUsers = async () => {
        try {
          const response = await axios.get('http://localhost:5000/api/users'); // Assuming the API endpoint is '/users'
          setUsers(response.data);
          console.log(response);
        } catch (error) {
          console.error('Failed to fetch users:', error);
        }
      };
      fetchUsers();
    }, []);


  
    useEffect(() => {
      const getDocuments = async () => {
        try {
          const res = await axios.get('http://localhost:5000/api/documents/');
          setDocuments(res.data);
          console.log(res);
        } catch (err) {
          console.error(err);
        }
      };
      getDocuments();
    }, []);

  
  const getSingleUser = async (userId) => {
    try {
      const response = await axios.get(`http://localhost:5000/api/users/find/${userId}`);
      const user = response.data;
  
      // Fetch user's documents
      const documentsResponse = await axios.get(`http://localhost:5000/api/documents/find/user/${userId}`);
      const documents = documentsResponse.data;
      
      // fetch user's memos

      const memoResponse = await axios.get(`http://localhost:5000/api/memo/find/user/${userId}`);
      const memos = memoResponse.data;

      setSelectedUser({ ...user, documents: documents, memos: memos });
    } catch (error) {
      console.error('Failed to fetch user:', error);
    }
  };
  

  const updateUser = async () => {
    try {
      const response = await axios.put(
        `http://localhost:5000/api/users/${selectedUser._id}`,
        selectedUser
      );
      console.log(response.data);
      toast.success('User updated successfully!');
    } catch (error) {
      console.error('Failed to update user:', error);
    }
  };

  useEffect(() => {
    const getMonthlyData = () => {
      const monthlyUsers = Array(12).fill(0); // Initialize an array with 12 elements for 12 months
      const monthlyMemos = Array(12).fill(0);
      const monthlyDocuments = Array(12).fill(0);
    
      // Calculate the monthly counts
      users.forEach((user) => {
        const month = new Date(user.createdAt).getMonth();
        monthlyUsers[month]++;
      });
    
      memos.forEach((memo) => {
        const month = new Date(memo.createdAt).getMonth();
        monthlyMemos[month]++;
      });
    
      documents.forEach((document) => {
        const month = new Date(document.createdDate).getMonth();
        monthlyDocuments[month]++;
      });
    
      return {
        monthlyUsers,
        monthlyDocuments,
        monthlyMemos,
      };
    };
    

    if (chartRef.current && documents.length > 0) {
      const { monthlyUsers, monthlyMemos, monthlyDocuments } = getMonthlyData();
      const chartCanvas = chartRef.current.getContext('2d');

      if (chartInstance) {
        // Destroy the previous Chart instance
        chartInstance.destroy();
      }

      const newChartInstance = new Chart(chartCanvas, {
        type: 'bar',
        data: {
          labels: [
            'January',
            'February',
            'March',
            'April',
            'May',
            'June',
            'July',
            'August',
            'September',
            'October',
            'November',
            'December',
          ],
          datasets: [
            {
              label: 'Users',
              data: monthlyUsers,
              backgroundColor: 'rgba(75, 192, 192, 0.6)',
              borderColor: 'rgba(75, 192, 192, 1)',
              borderWidth: 1,
            },
            {
              label: 'Memos',
              data: monthlyMemos,
              backgroundColor: 'rgba(54, 162, 235, 0.6)',
              borderColor: 'rgba(54, 162, 235, 1)',
              borderWidth: 1,
            },
            {
              label: 'Documents',
              data: monthlyDocuments,
              backgroundColor: 'rgba(255, 99, 132, 0.6)',
              borderColor: 'rgba(255, 99, 132, 1)',
              borderWidth: 1,
            },
          ],
        },
        options: {
          responsive: true,
          scales: {
            y: {
              beginAtZero: true,
            },
          },
        },
      });

      setChartInstance(newChartInstance);
    }
  }, [users, memos, documents]);


  const getMonthName = (monthIndex) => {
    const monthNames = [
      'January',
      'February',
      'March',
      'April',
      'May',
      'June',
      'July',
      'August',
      'September',
      'October',
      'November',
      'December',
    ];
    return monthNames[monthIndex];
  };

  const handlePrint = () => {
    const content = `
      Total Users: ${users.length}
      Total Memos: ${memos.length}
      Total Documents: ${documents.length}
    `;

    const file = new Blob([content], { type: 'text/plain;charset=utf-8' });
    saveAs(file, 'report.doc');
  };




  return (
    <>
      <Container fluid className="dashboard-container">
        <Row>
          <Col md={9} className="main-content-col text-dark" style={{ width: '99%' }}>
            <div className="scrollable-container">
              <Card className="main-card">
                <Card.Header className='bg-dark text-light'>
                <Card.Title className="main-title">Reports</Card.Title>
                </Card.Header>
                <Card.Body>
                 
                  <Button variant="dark" onClick={handlePrint}>
                    Print Report
                  </Button>
                  {documents.length > 0 ? (
                    <canvas ref={chartRef} id="chart" />
                  ) : (
                    <div>Loading chart...</div>
                  )}
                </Card.Body>
              </Card>
              <Row className="card-row mt-1">
                <Col md={4}>
                  <Card className="main-card">
                   
                  <Card.Body>
  <Card.Title className="main-title">Total Memos</Card.Title>
  <Card.Text className="main-text">{memos.length}</Card.Text>
</Card.Body>
</Card>
</Col>
<Col md={4}>
<Card className="main-card">
<Card.Body>
  <Card.Title className="main-title">Total Users</Card.Title>
  <Card.Text className="main-text">{users.length}</Card.Text>
</Card.Body>
</Card>
</Col>
<Col md={4}>
<Card className="main-card">
<Card.Body>
  <Card.Title className="main-title">Total Documents</Card.Title>
  <Card.Text className="main-text">{documents.length}</Card.Text>
</Card.Body>
</Card>
</Col>
</Row>
<Row className="card-row mt-1">
<Col md={6}>
          <Card className="main-card mt-1">
            <Card.Header className="bg-dark text-light">
              <Card.Title className="main-title">Users</Card.Title>
            </Card.Header>
            <Card.Body className="mt-2">
              <ListGroup variant="flush">
                {users.map((user) => (
                  <ListGroup.Item key={user._id} className="d-flex align-items-center">
                    <img
                      src={user.img}
                      style={{ width: '30px' }}
                      alt="User Avatar"
                      className="m-2 rounded"
                    />
                    {user.firstName} {user.lastName}
                    <BsPencilSquare
                      className="edit-icon ml-auto"
                      onClick={() => getSingleUser(user._id)}
                    />
                  </ListGroup.Item>
                ))}
              </ListGroup>
            </Card.Body>
          </Card>
        </Col>
        <Col md={6}>
          <Card className="main-card mt-2">
            <Card.Header className="bg-dark text-light">
              <Card.Title className="main-title">User details</Card.Title>
            </Card.Header>
            <Card.Body>
              {selectedUser && (
                <div>
                   <Form.Group controlId="formFirstName">
                    <Form.Label>First Name</Form.Label>
                    <Form.Control
                      type="text"
                      value={selectedUser.firstName}
                      onChange={(e) =>
                        setSelectedUser({
                          ...selectedUser,
                          firstName: e.target.value,
                        })
                      }
                    />
                  </Form.Group>

                  <Form.Group controlId="formLastName">
                    <Form.Label>Last Name</Form.Label>
                    <Form.Control
                      type="text"
                      value={selectedUser.lastName}
                      onChange={(e) =>
                        setSelectedUser({
                          ...selectedUser,
                          lastName: e.target.value,
                        })
                      }
                    />
                  </Form.Group>

                  <Form.Group controlId="formUsername">
                    <Form.Label>Username</Form.Label>
                    <Form.Control
                      type="text"
                      value={selectedUser.username}
                      onChange={(e) =>
                        setSelectedUser({
                          ...selectedUser,
                          username: e.target.value,
                        })
                      }
                    />
                  </Form.Group>

                  <Form.Group controlId="formPassword">
                    <Form.Label>Password</Form.Label>
                    <Form.Control
                      type="text"
                      value={selectedUser.password}
                      onChange={(e) =>
                        setSelectedUser({
                          ...selectedUser,
                          password: e.target.value,
                        })
                      }
                    />
                  </Form.Group>

                  <Form.Group controlId="formEmail">
                    <Form.Label>Email</Form.Label>
                    <Form.Control
                      type="text"
                      value={selectedUser.email}
                      onChange={(e) =>
                        setSelectedUser({
                          ...selectedUser,
                          email: e.target.value,
                        })
                      }
                    />
                  </Form.Group>

                  <Form.Group controlId="formRole">
                      <Form.Label>Role</Form.Label>
                      <Form.Select
                        value={selectedUser.role}
                        onChange={(e) =>
                          setSelectedUser({
                            ...selectedUser,
                            role: e.target.value,
                          })
                        }
                      >
                        <option value="">{selectedUser.role}</option>
                        <option value="Standard">standard</option>
                        <option value="Creator">Creator</option>
                      </Form.Select>
                    </Form.Group>


                  <Form.Group controlId="formLocation">
                    <Form.Label>Location</Form.Label>
                    <Form.Select
                      value={selectedUser.location}
                      onChange={(e) =>
                        setSelectedUser({
                          ...selectedUser,
                          location: e.target.value,
                        })
                      }
                    >
                      <option value="">Select a location</option>
                      <option value="Citynet">Citynet</option>
                      <option value="Summit">Summit</option>
                      <option value="Sun Plaza">Sun Plaza</option>
                    </Form.Select>
                  </Form.Group>


                  <Form.Group controlId="formImage">
                    <Form.Label>Image</Form.Label>
                    <Form.Control
                      type="text"
                      value={selectedUser.img}
                      onChange={(e) =>
                        setSelectedUser({
                          ...selectedUser,
                          img: e.target.value,
                        })
                      }
                    />
                  </Form.Group>

                  {selectedUser.documents && (
                    <p>Document count: {selectedUser.documents.length}</p>
                  )}

                  {selectedUser.memos && (
                    <p>Memo count: {selectedUser.memos.length}</p>
                  )}

                  {/* Add more input fields for other user details */}
                  <Button className='btn btn-dark' onClick={updateUser}>Save</Button>
                </div>
              )}
            </Card.Body>
          </Card>
        </Col>
      </Row>  
</div>
</Col>
</Row>
</Container>
</>
);
};

export default DashboardContents;
