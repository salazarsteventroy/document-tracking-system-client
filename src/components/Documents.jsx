import React, { useEffect, useState } from 'react';
import { Card, Navbar, Nav } from 'react-bootstrap';
import axios from "axios";
import { useNavigate } from 'react-router-dom';
import TopBar from './TopBar';
import { useLocation } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { publicRequest } from '../requestMethods';


const Docs = () => {
  const location = useLocation();
  const [documents, setDocuments] = useState([]);
  const [filteredDocuments, setFilteredDocuments] = useState([]);
  const navigate = useNavigate();
  const [status, setStatus] = useState(null);
  const [searchTerm, setSearchTerm] = useState('');
  const id = location.pathname.split("/")[2];
  const userId = useSelector((state) =>  state.user._id );
  //const user = useSelector((state) => state.user.firstName);
  const userRole = useSelector((state) => state.user.role);
  const isAdmin = useSelector((state) => state.user.isAdmin);


  // useEffect(() => {
  //   const fetchDocuments = async () => {
  //     try {
  //       const response = await axios.get('http://localhost:5000/api/documents');
  //       setDocuments(response.data);
  //       setFilteredDocuments(response.data);
  //     } catch (error) {
  //       console.error(error);
  //     }
  //   };

  //   fetchDocuments();
  // }, []);

  // useEffect(() => {
  //   const fetchDocuments = async () => {
  //     try {
  //       const response = await axios.get('http://localhost:5000/api/documents');
  //       setDocuments(response.data);
  //       setFilteredDocuments(response.data);
  //     } catch (error) {
  //       console.error(error);
  //     }
  //   };

  //   fetchDocuments();
  // }, []);

  const markDocumentAsOpened = async (documentId) => {
    try {
      await axios.put(`http://localhost:5000/api/documents/${documentId}`, { opened: true });
      // Optional: You can perform any additional actions after marking the document as opened.
      setDocuments((prevDocuments) =>
        prevDocuments.map((doc) =>
          doc._id === documentId ? { ...doc, opened: true } : doc
        )
      );
    } catch (error) {
      console.error(error);
    }
  };



  useEffect(() => {
    const getDocuments = async () => {
      try {
        let url = 'http://localhost:5000/api/documents';
  
        if (userRole === 'standard' && !isAdmin) {
          // Assuming you have the user ID stored in a variable called `userId`
          url = `http://localhost:5000/api/documents/find/user/${userId}`;
        } else if (status) {
          url = `http://localhost:5000/api/documents?status=${status}`;
        }

        const res = await axios.get(url);
        const sortedDocuments = res.data.sort((a, b) => new Date(b.createdDate) - new Date(a.createdDate));
        console.log(res);
        setDocuments(sortedDocuments);
      } catch (err) {
        console.error(err);
      }
    };
    getDocuments();
  }, [userRole, isAdmin, userId, status]);
  

  useEffect(() => {
    let filtered = documents;
  
    if (status) {
      filtered = filtered.filter((item) => item.status === status);
    }
  
    if (searchTerm) {
      filtered = filtered.filter((item) =>
        item.title.toLowerCase().includes(searchTerm.toLowerCase())
      );
    }
  
    setFilteredDocuments(filtered);
  }, [documents, status, searchTerm]);
  

  return (
    <>
    <div
      style={{marginRight:"-295px", position:"relative"}}
    >
     <TopBar documents={documents} setFilteredDocuments={setFilteredDocuments} />
     </div>
    <div className="vh-100"
      style={{minWidth:"100vh"}}
    >
      <Nav
        className="bg-dark text-light d-flex justify-content-around rounded"
        style={{ padding: "5px", marginBottom: "5px" }}
      >
        <Navbar.Brand className="mr-3 text-muted">|</Navbar.Brand>
        <Navbar.Brand href="#All"   className={status === "" ? "mr-3 text-light" : "mr-3 text-muted"}
          onClick={() => setStatus("")}
        >
          All
        </Navbar.Brand>
        <Navbar.Brand className="mr-3 text-muted">|</Navbar.Brand>
        <Navbar.Brand
          href="#ongoing"
          className={status === "ongoing" ? "mr-3 text-light" : "mr-3 text-muted"}
          onClick={() => setStatus("ongoing")}
        >
          Ongoing
        </Navbar.Brand>
        <Navbar.Brand className="mr-3 text-muted">|</Navbar.Brand>
        <Navbar.Brand
          href="#completed"
          className={status === "completed" ? "mr-3 text-light" : "mr-3 text-muted"}
          onClick={() => setStatus("completed")}
        >
          Completed
        </Navbar.Brand>
        <Navbar.Brand className="mr-3 text-muted">|</Navbar.Brand>
        <Navbar.Brand
          href="#archived"
          className={status === "archived" ? "mr-3 text-light" : "mr-3 text-muted"}
          onClick={() => setStatus("archived")}
        >
          Archived
        </Navbar.Brand>
        <Navbar.Brand className="mr-3 text-muted">|</Navbar.Brand>
      </Nav>

      <div
        style={{
          overflowY: "auto",
          maxHeight: "calc(104vh - 150px)",
        }}
      >
        {filteredDocuments.length > 0 ? (
          filteredDocuments.map((doc) => (
            <Card
              className="mx-auto border-dark bg-dark"
              style={{ marginBottom: "3px", width: "50rem", cursor: "pointer", transition: "transform 0.2s ease-out", minWidth:"99%" }}
              key={doc.id}
              onClick={() => {
                markDocumentAsOpened(doc._id);
                navigate(`/document/${doc._id}`);
              }}
              onMouseEnter={(e) => e.target.style.transform = "scale(1.01)"}
              onMouseLeave={(e) => e.target.style.transform = "scale(1)"}
              >
              <Card.Header>
                <Card.Title>
                  <h6 className="outline-light text-warning mt-2">{doc.title}</h6>
                </Card.Title>
              </Card.Header>
              <Card.Body className="d-flex justify-content-between">
                <div className="d-flex flex-column">
                  <Card.Subtitle className="mb-2 text-muted">
                    {doc.docType + " "}
                    {doc.trackingNo}
                  </Card.Subtitle>
                  <Card.Text className="text-light">
                    {doc.description}
                  </Card.Text>
                  <Card.Text className="text-success">{doc.status}</Card.Text>
                </div>
                <Card.Footer className="align-self-end">
                  <small className="text-muted">
                    Created by {doc.createdBy} on {doc.createdDate}
                
                  </small>
                </Card.Footer>
              </Card.Body>
            </Card>
          ))
        ) : (
          <p>No documents found.</p>
        )}
      </div>
    </div>
    </>
  );
};

export default Docs;
