import { useEffect, useState } from "react";
import { Form, Button, Alert, Modal } from "react-bootstrap";
import axios from "axios";
import { useLocation } from 'react-router-dom';
import { useSelector } from "react-redux";
import { publicRequest } from "../requestMethods";

const ReplyModal = () => {
  const location = useLocation();
  const [subject, setSubject] = useState("");
  const [reply, setReply] = useState("");
  const [error, setError] = useState(null);
  const [replyLocation, setReplyLocation] = useState("");
  const [showModal, setShowModal] = useState(false);
  const id = location.pathname.split("/")[2];
  const userLocation = useSelector((state) => state.user.location);
  const [logTitle, setLogTitle] = useState("");
  const [logDescription, setLogDescription] = useState("");
  const [docId, setLogId] = useState("");
  const [replyRecipient, setReplyRecipient] = useState("");
  const userEmail = useSelector((state) => state.user.email);
  const [document, setDocument] = useState({});


  useEffect(() => {
    const getDocument = async () => {
      try {
        const res = await publicRequest.get("/documents/find/" + id);
        setDocument(res.data);
      } catch (error) {
        console.error(error);
      }
    };
    getDocument();
  }, [id]);



  const lastUpdatedTime = new Date(document.lastUpdatedBy);
  const currentTime = new Date();
  const elapsedMilliseconds = currentTime - lastUpdatedTime;
  const elapsedSeconds = Math.floor(elapsedMilliseconds / 1000) % 60; // Use modulo operator
  const elapsedMinutes = Math.floor(elapsedSeconds / 60) % 60;
  const elapsedHours = Math.floor(elapsedMinutes / 60) % 24;
  const elapsedDays = Math.floor(elapsedHours / 24);
  const elapsedTimeString = getElapsedTimeString(elapsedDays, elapsedHours, elapsedMinutes, elapsedSeconds);
  const currentDateTimeString = getCurrentDateTimeString();

  function getCurrentDateTimeString() {
    const currentDateTime = new Date();
    const options = {
      year: 'numeric',
      month: 'long',
      day: 'numeric',
      hour: 'numeric',
      minute: 'numeric',
      second: 'numeric',
      hour12: false
    };
    return currentDateTime.toLocaleString(undefined, options);
  }

  function getElapsedTimeString(days, hours, minutes, seconds) {
    let elapsedTimeString = '';
    if (days > 0) {
      elapsedTimeString += `${days} day${days > 1 ? 's' : ''} `;
    }
    if (hours > 0) {
      elapsedTimeString += `${hours} hour${hours > 1 ? 's' : ''} `;
    }
    if (minutes > 0) {
      elapsedTimeString += `${minutes} minute${minutes > 1 ? 's' : ''} `;
    }
    if (seconds > 0) {
      elapsedTimeString += `${seconds} second${seconds > 1 ? 's' : ''}`;
    }
    return elapsedTimeString.trim();
  }



  const handleSend = async (e) => {
    e.preventDefault();
    if (subject === "" || reply === "" || replyLocation === "") {
      setError("Please fill in all required fields.");
    } else {
      try {
        const response = await axios.post(`http://localhost:5000/api/documents/${id}/reply`, {
            reply,
            subject,
            replyLocation,
            replyRecipient
        });



        await publicRequest.put(`/documents/${id}`, { currentHolder: replyRecipient });
        setDocument((prevDocument) => ({
          ...prevDocument,
          currentHolder: replyRecipient
        }));




        if (response.status === 200) {
          const logs = await axios.post(`http://localhost:5000/api/documents/${id}/logs`, {
            logTitle: `Document being transfered on ${new Date().toLocaleString()} to ${replyLocation}`,
            logDescription: ` Transfered by : ${userEmail} time elapsed ${elapsedTimeString}`,
            docId: response.data._id,
          });

          console.log(logs);

          setLogTitle(`Document being transfered on ${new Date().toLocaleString()} to ${replyLocation}`);
          setLogDescription(` Transfered by : ${userEmail} `);
          setLogId(response.data._id);

          // Clear the reply form
          setSubject("");
          setReply("");
          setError(null);

          setShowModal(true);
        } else {
          setError("An error occurred. Please try again.");
        }
      } catch (error)  {
        if (error.response || error.response.data || error.response.data.message || error.response.data.error) {
          setError(error.response.data.error); 
        } else {
          setError("Sender was not found");
        }
      }
    }
  };

  return (
    <>
      <Form>

      <Form.Group className="mb-3">
          <Form.Label>To:</Form.Label>
          <Form.Control
            type="text"
            placeholder="Enter Recipient's Name"
            value={replyRecipient}
            onChange={(e) => setReplyRecipient(e.target.value)}
          />
        </Form.Group>

      <Form.Group className="mb-3">
          <Form.Label>Location:</Form.Label>
          <Form.Control
            type="text"
            placeholder="Enter Location"
            value={replyLocation}
            onChange={(e) => setReplyLocation(e.target.value)}
          />
        </Form.Group>

        <Form.Group className="mb-3">
          <Form.Label>Subject*</Form.Label>
          <Form.Control
            type="text"
            placeholder="Enter subject"
            value={subject}
            onChange={(e) => setSubject(e.target.value)}
          />
        </Form.Group>
        <Form.Group className="mb-3">
          <Form.Label>Reply*</Form.Label>
          <Form.Control
            as="textarea"
            rows={3}
            placeholder="Enter reply"
            value={reply}
            onChange={(e) => setReply(e.target.value)}
          />
        </Form.Group>
        {error && (
          <Alert variant="danger" style={{ padding: "2px", textAlign: "center" }}>
            {error}
          </Alert>
        )}
        <Button variant="dark" type="submit" onClick={handleSend} block>
          Send
        </Button>
      </Form>
      <Modal show={showModal} onHide={() => setShowModal(false)}>
        <Modal.Header closeButton>
          <Modal.Title>Reply Sent Successfully!</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          Your reply has been sent successfully.<br />
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={() => setShowModal(false)}>
            Close
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
};

export default ReplyModal;
