import { useState } from "react";
import { Container, Row, Col, Form, Button, Alert, Modal } from "react-bootstrap";
import axios from "axios";
import { useSelector } from "react-redux";

const SendDocumentModal = () => {
  const [docType, setDocumentType] = useState("Select a document type");
  const [recipient, setRecipient] = useState("");
  const [remarks, setRemarks] = useState("");
  const [message, setMessage] = useState("");
  const [error, setError] = useState(null);
  const [showModal, setShowModal] = useState(false);
  const [customDocumentType, setCustomDocumentType] = useState("");
  const [title, setTitle] = useState("");
  const [description, setDescription] = useState("");
  const [location, setLocation] = useState("");
  const [transType, setTransType] = useState("");
  const [createdBy, setCreatedBy] = useState("");
  const userFirstName = useSelector((state) => state.user.firstName);
  const userLastName = useSelector((state) => state.user.lastName);
  const userLocation = useSelector((state) => state.user.location);

  const handleSend = async (e) => {
    e.preventDefault();
    if (docType === "Select a document type" || recipient === "" || message === "") {
      setError("Please fill in all required fields.");
    } else {
      try {
        const createdByCreator = `${userFirstName} ${userLastName}`;
        const response = await axios.post("http://localhost:5000/api/documents", {
          docType: docType === "Other" ? customDocumentType : docType,
          recipient,
          remarks,
          message,
          title,
          description,
          location,
          transType,
          createdBy,
          createdByCreator,
        });
        
        if (response.status === 200) {
          const logs = await axios.post(`http://localhost:5000/api/documents/${response.data._id}/logs`, {
            logTitle: `Document created on ${new Date().toLocaleString()} in ${userLocation}`,
            logDescription: `Created by: ${createdByCreator}`,
            docId: response.data._id,
          });

          console.log("Logs:", logs.data);

          setShowModal(true);
          setDocumentType("Select a document type");
          setRecipient("");
          setRemarks("");
          setMessage("");
          setError(null);
          setTitle("");
          setDescription("");
          setLocation("");
          setTransType("");
          setCreatedBy("");
        }
      } catch (error) {
        // Handle error
        if (error.response || error.response.data || error.response.data.message || error.response.data.error) {
          setError(error.response.data.error);
        } else {
          setError("Recipient/Sender was not found");
        }
      }
    }
  };

  console.log("State Values:", {
    docType,
    recipient,
    remarks,
    message,
    error,
    showModal,
    customDocumentType,
    title,
    description,
    location,
    transType,
    createdBy,
    userFirstName,
    userLastName,
  });

  return (
    <>
      <Container>
        <Row className="w-100">
          <Col className="bg-light p-4 rounded shadow">
            <Form>
              <Form.Group className="mb-3">
                <Form.Label>Document Type*</Form.Label>
                <Form.Select
                  value={docType}
                  onChange={(e) => {
                    const selectedValue = e.target.value;
                    setDocumentType(selectedValue);
                    if (selectedValue === "Other") {
                      setCustomDocumentType(""); // Reset the custom document type
                    }
                  }}
                >
                  <option value="Select a document type">Select a document type</option>
                  <option value="General Document">General Document</option>
                  <option value="Cheque">Cheque</option>
                  <option value="Other">Other</option>
                </Form.Select>
                {docType === "Other" && (
                  <Form.Group controlId="formCustomDocumentType">
                    <Form.Label>Custom Document Type</Form.Label>
                    <Form.Control
                      type="text"
                      value={customDocumentType}
                      onChange={(e) => setCustomDocumentType(e.target.value)}
                    />
                  </Form.Group>
                )}
              </Form.Group>
              <Form.Group className="mb-3">
                <Form.Label>Recipient*</Form.Label>
                <Form.Control
                  type="text"
                  placeholder="Enter recipient's email address"
                  value={recipient}
                  onChange={(e) => setRecipient(e.target.value)}
                />
              </Form.Group>
              <Form.Group className="mb-3">
                <Form.Label>From</Form.Label>
                <Form.Control
                  type="text"
                  placeholder="Enter sender's email"
                  value={createdBy}
                  onChange={(e) => setCreatedBy(e.target.value)}
                />
              </Form.Group>
              <Form.Group className="mb-3">
                <Form.Label>Title</Form.Label>
                <Form.Control
                  type="text"
                  placeholder="Enter document title"
                  value={title}
                  onChange={(e) => setTitle(e.target.value)}
                />
              </Form.Group>
              <Form.Group className="mb-3">
                <Form.Label>Message*</Form.Label>
                <Form.Control
                  as="textarea"
                  rows={3}
                  placeholder="Enter message"
                  value={message}
                  onChange={(e) => setMessage(e.target.value)}
                />
              </Form.Group>
              <Form.Group className="mb-3">
                <Form.Label>Remarks</Form.Label>
                <Form.Control
                  type="text"
                  placeholder="Enter remarks (optional)"
                  value={remarks}
                  onChange={(e) => setRemarks(e.target.value)}
                />
              </Form.Group>

              <Form.Group className="mb-3">
                <Form.Label>Description</Form.Label>
                <Form.Control
                  type="text"
                  placeholder="Enter document description"
                  value={description}
                  onChange={(e) => setDescription(e.target.value)}
                />
              </Form.Group>
              <Form.Group className="mb-3">
                <Form.Label>Location</Form.Label>
                <Form.Select
                  value={location}
                  onChange={(e) => setLocation(e.target.value)}
                >
                  <option value="">Select a location</option>
                  <option value="Sun Plaza">Sun Plaza</option>
                  <option value="Summit">Summit</option>
                  <option value="Citynet">Citynet</option>
                </Form.Select>
              </Form.Group>
              <Form.Group className="mb-3">
                <Form.Label>Transaction Type</Form.Label>
                <Form.Select
                  value={transType}
                  onChange={(e) => setTransType(e.target.value)}
                >
                  <option value="">Select transaction type</option>
                  <option value="One Way">One Way</option>
                  <option value="Round Trip">Round Trip</option>
                </Form.Select>
              </Form.Group>

              {error && (
                <Alert variant="danger" style={{ padding: "2px", textAlign: "center" }}>
                  {error}
                </Alert>
              )}
              <Button variant="dark" type="submit" onClick={handleSend} block>
                Send
              </Button>
            </Form>
          </Col>
        </Row>
      </Container>
      <Modal show={showModal} onHide={() => setShowModal(false)}>
        <Modal.Header closeButton>
          <Modal.Title>Document Sent Successfully!</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          Your document has been sent successfully
           {/* to {recipient}.<br />
          <strong>Document Type:</strong> {docType}<br />
          <strong>Remarks:</strong> {remarks}<br />
          <strong>Message:</strong> {message}<br />
          <strong>Title:</strong> {title}<br />
          <strong>Description:</strong> {description}<br />
          <strong>Location:</strong> {location}<br />
          <strong>Transaction Type:</strong> {transType}<br /> */}
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={() => setShowModal(false)}>
            Close
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
};

export default SendDocumentModal;

