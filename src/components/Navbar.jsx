import React from 'react';
import { Navbar, Nav, Form, FormControl, Button } from 'react-bootstrap';
import { FaAlignJustify, FaSearch, FaEnvelope, FaUser, FaFile, FaSignOutAlt, FaFolderOpen  } from 'react-icons/fa';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../scss/custom.scss';
import { Link } from "react-router-dom";
import { useState } from 'react';
import { Modal } from 'react-bootstrap';
import SendDocumentModal from './sendDocumentModal';
import UserSettingsModal from './userSettingsModal';
import { useSelector, useDispatch } from 'react-redux';
import { logout } from '../redux/userRedux';

function NavigationBar() {
  const [showDocumentModal, setShowDocumentModal] = useState(false);
  const [showUserModal, setShowUserModal] = useState(false);

  const handleOpenDocumentModal = () => setShowDocumentModal(true);
  const handleCloseDocumentModal = () => setShowDocumentModal(false);

  const handleOpenUserModal = () => setShowUserModal(true);
  const handleCloseUserModal = () => setShowUserModal(false);

  const user = useSelector((state) => state.user.firstName);
  const userLastname = useSelector((state) => state.user.lastName);
  const userImage = useSelector((state) => state.user.img);
  const userRole = useSelector((state) => state.user.role);
  const isAdmin = useSelector((state) => state.user.isAdmin);

  const dispatch = useDispatch();

  const handleDelete = (e) => {
    e.preventDefault();
    dispatch(logout());
  };

  return (
    <div style={{ display: 'flex', height: '90vh', justifyContent: 'center' }}>
      <div className="d-flex flex-column text-dark border border-secondary rounded bg-dark" style={{ flexBasis: '90%', alignItems: 'center' }}>
        <div className="d-flex">
          <Button className="mt-3 rounded-circle p-0" variant="outline-dark" style={{ width: '120px', height: '120px' }}>
            <img src={userImage} alt="User" style={{ borderRadius: '50%', width: '100%', height: '100%', objectFit: 'cover' }} />
          </Button>
        </div>
        <br />
        <Button className='text-light' variant='outline-dark'>{user} {userLastname}</Button>
        <div>
          <Nav className="flex-column mt-3">
            <div>
              {userRole !== 'standard' || isAdmin ? (
                              <Button variant="outline-light" onClick={handleOpenDocumentModal}>
                              <FaEnvelope /> Send Document
                            </Button>
                  ) : null }
              <Modal show={showDocumentModal} onHide={handleCloseDocumentModal}>
                <Modal.Header closeButton>
                  <Modal.Title>Send a Document</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                  <SendDocumentModal onClose={handleCloseDocumentModal} />
                </Modal.Body>
              </Modal>
            </div>
            <Link to="/" style={{ textDecoration: 'none', color: 'inherit' }}>
              <Button className="mt-4" variant="outline-light" style={{ width: '100%' }}>
                 <FaFolderOpen  /> Documents
              </Button>
            </Link>
            <Link to="/memos" style={{ textDecoration: 'none', color: 'inherit' }}>
              <Button className="mt-4" variant="outline-light" style={{ width: '100%' }}>
                <FaFile /> Memo
              </Button>
            </Link>
            {userRole === 'standard' && isAdmin ? (
              <Link to="/dashboard" style={{ textDecoration: 'none', color: 'inherit' }}>
                <Button className="mt-4" variant="outline-light" style={{ width: '100%' }}>
                  <FaAlignJustify /> Dashboard
                </Button>
              </Link>
            ) : null}
            <Button className="mt-4" variant="outline-light" onClick={handleOpenUserModal}>
              <FaUser /> Configure User
            </Button>
            <Modal show={showUserModal} onHide={handleCloseUserModal}>
              <Modal.Header className='bg-light text-dark' closeButton>
                <Modal.Title>
                  Configure User
                </Modal.Title>
              </Modal.Header>
              <Modal.Body>
                <UserSettingsModal onClose={handleCloseUserModal} />
              </Modal.Body>
            </Modal>
            <br />
            <br />
            <br />
            <br />
            <br />
            <Button className="mt-4" variant="outline-light" onClick={handleDelete}>
              <FaSignOutAlt /> Logout
            </Button>
          </Nav>
        </div>
      </div>
    </div>
  );
}

export default NavigationBar;
