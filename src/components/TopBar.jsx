import React, { useState } from 'react';
import { Navbar, Nav, Form, FormControl, Button } from 'react-bootstrap';
import { FaSearch } from 'react-icons/fa';
import 'bootstrap/dist/css/bootstrap.min.css';
import { useLocation } from 'react-router-dom';

function TopBar({ documents, setFilteredDocuments }) {
  const location = useLocation();
  const [searchTerm, setSearchTerm] = useState('');

  const handleSearch = (e) => {
    e.preventDefault();
    const filtered = documents.filter((doc) =>
      doc.title.toLowerCase().includes(searchTerm.toLowerCase()) || doc.trackingNo.toLowerCase().includes(searchTerm.toLowerCase()) || doc.createdBy.toLowerCase().includes(searchTerm.toLowerCase()) || doc.recipient.toLowerCase().includes(searchTerm.toLowerCase()) || doc.description.toLowerCase().includes(searchTerm.toLowerCase())
    );
    setFilteredDocuments(filtered);
  };

  // Check if the current location is not "/"
  const isHomePage = location.pathname === "/";
  const isMemoPage = location.pathname === "/memos";
  const isDashboardPage = location.pathname === "/dashboard";

  return (
    <div style={{ marginRight: '6rem', display: 'flex', justifyContent: 'flex-end', flexGrow: 1 }}>
      <Navbar expand="lg">
        <Navbar.Brand href="/">
          {isHomePage && "Documents"}
          {isMemoPage && "Memos"}
          {isDashboardPage && "Dashboard"}
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          {isHomePage && (
            <Form inline onSubmit={handleSearch}>
              <div className="d-flex">
                <FormControl
                  type="text"
                  placeholder="Search"
                  className="mr-sm-2"
                  value={searchTerm}
                  onChange={(e) => setSearchTerm(e.target.value)}
                />
                <Button variant="outline-dark" type="submit">
                  <FaSearch />
                </Button>
              </div>
            </Form>
          )}
        </Navbar.Collapse>
      </Navbar>
    </div>
  );
}

export default TopBar;
