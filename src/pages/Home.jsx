import NavigationBar from "../components/Navbar";
import Documents from "../components/Documents";
import { useEffect, useState } from "react";
import { publicRequest } from "../requestMethods";
import React from 'react';
import TopBar from "../components/TopBar";
import "../css/home.scss";
import Notificationbar from "../components/notificationbar";


function Home() {
  return (
    

    <div className="home-container">
     
      <div className="page-container">
        <div className="sidebar-container">
          <NavigationBar />
        </div>
        <div className="content-container">
          <Documents />
        </div>
        <div className="notification-container">
          <Notificationbar/>
        </div>
      </div>
    </div>

  );
}

export default Home;