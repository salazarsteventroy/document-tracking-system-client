import { useState } from "react";
import { useDispatch } from "react-redux";
import { login } from "../redux/apiCalls";
import { Container, Row, Col, Form, Button, Alert } from "react-bootstrap";
import "../css/login.css";
import { Link, useNavigate } from "react-router-dom";
import axios from "axios";

const Login = () => {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [error, setError] = useState(null); // added state for error message
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const handleLogin = async (e) => {
    e.preventDefault();
    try {
      const response = await login(dispatch, { username, password });

      if (response.status === 401) {
        setError("Incorrect username or password");
      } else {
        // Set headers with access token
        const currentUser = response.data;
        const accessToken = currentUser.accessToken;
        axios.defaults.headers.common["Authorization"] = `Bearer ${accessToken}`;

        // Additional logic after successful login
        // For example, redirect to dashboard or perform other actions
        navigate("/");
      }
    } catch (error) {
      console.log(error);
      setError("Wrong username or password.");
    }
  };

  const handleRegister = () => {
    // handle register logic here
  };

  return (
    <div className="Container5">
      <Container fluid className="d-flex align-items-center justify-content-center" style={{ height: "100vh" }}>
        <Row className="w-100">
          <Col md={{ span: 6, offset: 3 }} lg={{ span: 4, offset: 4 }} className="bg-dark p-4 rounded shadow">
            <h3 className="mb-4 text-light">Sign In</h3>
            <Form>
              <Form.Group className="mb-3">
                <Form.Control type="text" placeholder="Username" value={username} onChange={(e) => setUsername(e.target.value)} />
              </Form.Group>
              <Form.Group className="mb-3">
                <Form.Control type="password" placeholder="Password" value={password} onChange={(e) => setPassword(e.target.value)} />
              </Form.Group>
              {error && (
                <Alert variant="danger" style={{ padding: "2px", textAlign: "center" }}>
                  {error}
                </Alert>
              )}
              <Button variant="light" type="submit" onClick={handleLogin} disabled={!username || !password} block>
                Login
              </Button>
              <Link to="/register" style={{ color: "gray", marginLeft: "180px", position: "relative" }}>
                Don't have an account?
              </Link>
            </Form>
          </Col>
        </Row>
      </Container>
    </div>
  );
};

export default Login;
