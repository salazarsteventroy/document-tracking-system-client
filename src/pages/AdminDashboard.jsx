import React from 'react';
import TopBar from "../components/TopBar";
import NavigationBar from "../components/Navbar";
import DashboardContents from "../components/DashboardContents";
import { toast, ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

function adminDashboard() {
  return (
    <>
      <TopBar />
      <div style={{ display: 'flex', flexDirection: 'column', height: '100vh' }}>
        <div style={{ display: 'flex', flex: 1 }}>
          <div style={{ width: '250px' }}>
            <NavigationBar style={{ marginTop: '60px' }} />
          </div>
          <div style={{ flex: 1, display: 'flex', flexDirection: 'column'}}>
          <ToastContainer />
            <DashboardContents style={{ flex: 1, width: 'calc(100% - 250px)' }} />
          </div>
        </div>
      </div>
    </>
  );
}

export default adminDashboard;
