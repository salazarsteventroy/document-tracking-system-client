import NavigationBar from "../components/Navbar";
import MemoComponent from "../components/MemoComponent";
import { useEffect, useState } from "react";
import { publicRequest } from "../requestMethods";
import React from 'react';
import TopBar from "../components/TopBar";
import "../css/home.scss";
import Notificationbar from "../components/notificationbar";


function Memo() {
  return (
    

    <div className="home-container">
     
      <div className="page-container">
        <div className="sidebar-container">
          <NavigationBar />
        </div>
        <div className="content-container">
          <MemoComponent />
        </div>
        <div className="notification-container">
          <Notificationbar/>
        </div>
      </div>
    </div>

  );
}

export default Memo;