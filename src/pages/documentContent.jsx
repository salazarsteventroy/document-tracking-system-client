import React, { useEffect } from 'react';
import { BsReply, BsCheck, BsClock } from 'react-icons/bs';
import 'bootstrap/dist/css/bootstrap.min.css';
import { publicRequest } from '../requestMethods';
import { useLocation } from 'react-router-dom';
import { useState } from 'react';
import TopBar from '../components/TopBar';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import "../css/home.scss";
import ReplyModal from '../components/ReplyComponent';
import { Modal } from 'react-bootstrap';
import { useSelector, useDispatch } from 'react-redux';
import axios from 'axios';
import { FaEnvelope  } from 'react-icons/fa';


function DocumentContent() {
  const location = useLocation();
  const id = location.pathname.split("/")[2];
  const [document, setDocument] = useState({});
  const [logTitle, setLogTitle] = useState("");
  const [logDescription, setLogDescription] = useState("");
  const [docId, setLogId] = useState("");
  const [docLocation, setDoclocation] = useState({});
  const user = useSelector((state) => state.user.email);
  const userId = useSelector((state) => state.user._id);
  const userRole = useSelector((state) => state.user.role);
  const isAdmin = useSelector((state)=> state.user.isAdmin);
  const userLocation = useSelector((state) => state.user.location);
  const name = useSelector((state) => state.user.firstName);
  const lastname = useSelector((state) => state.user.lastName);

  

  const [showReplyModal, setShowReplyModal] = useState(false);
  const [showFollowUpModal, setShowFollowUpModal] = useState(false);

  const handleOpenReplyModal = () => setShowReplyModal(true);
  const handleCloseReplyModal = () => setShowReplyModal(false);



  const handleFollowUp = async () => {
    if (document.status === 'completed') {
      toast.info('This transaction is already completed.');
      return;
    }
  
    try {
      // await publicRequest.put(`/documents/${id}`, { status: 'follow-up' });
      // setDocument((prevDocument) => ({
      //   ...prevDocument,
      //   status: 'follow-up'
      // }));

      // Call the backend API to send the email to the appropriate recipient
      const response = await publicRequest.post(`/sendMail`, {
        to: document.createdBy === 'user' ? document.recipient : document.createdBy,
        subject: `Follow-up Required on ${document.title} ${document.trackingNo}`,
        text: `Please follow up on the document. ${document.title} ${document.description} ${document.trackingNo}`
      });
      // Handle the response from the email sending API if needed
  
      // Create the log entry for the document completion
      const logs = await axios.post(`http://localhost:5000/api/documents/${id}/logs`, {
        logTitle: `Followed up on ${new Date().toLocaleString()}`,
        logDescription: `by ${user}`,
        docId: userId
      });
  

      toast.success('Follow-up initiated successfully.');

      console.log(logs);
  
      setLogTitle(`Follow-up Initiated on ${new Date().toLocaleString()}`);
      setLogDescription(user);
      setLogId(userId);
    } catch (error) {
      console.error(error);
      toast.error('Failed to initiate follow-up.');
    }
  };
  
  const handleDone = async () => {
    if (document.status === 'completed') {
      toast.info('This transaction is already completed.');
      return;
    }
    
    try {

        // Update the document status to 'completed' using the publicRequest.put method
    await publicRequest.put(`/documents/${id}`, { status: 'completed' });

    // Update the document state with the updated status
    setDocument((prevDocument) => ({
      ...prevDocument,
      status: 'completed'
    }));
      
      toast.success('Document marked as completed successfully.');
  
      // Create the log entry for the document completion
      const logs = await publicRequest.post(`/documents/${id}/logs`, {
        logTitle: `Document Completed on ${new Date().toLocaleString()}`,
        logDescription: `by ${user}`,
        docId: userId
      });
  
      console.log(logs);
  
      setLogTitle(`Document Completed on ${new Date().toLocaleDateString()}`);
      setLogDescription(user);
      setLogId(userId);
    } catch (error) {
      console.error(error);
      toast.error('Failed to update the document.');
    }
  };
  
  const getCurrentDateTimeString = () => {
    const currentDateTime = new Date();
    const options = {
      year: 'numeric',
      month: 'long',
      day: 'numeric',
      hour: 'numeric',
      minute: 'numeric',
      second: 'numeric',
      hour12: false
    };
    return currentDateTime.toLocaleString(undefined, options);
  };
  
  useEffect(() => {
    const getDocument = async () => {
      try {
        const res = await publicRequest.get("/documents/find/" + id);
        setDocument(res.data);
      } catch (error) {
        console.error(error);
      }
    };
    getDocument();
  }, [id]);
  
  

  const handleLocation = async () => {
    if (document.status === 'completed') {
      toast.info('This transaction is already completed.');
      return;
    }
  
    try {
      const lastUpdatedTime = new Date(document.lastUpdatedBy);
      const currentTime = new Date();
      const elapsedMilliseconds = currentTime - lastUpdatedTime;
      const elapsedSeconds = Math.floor(elapsedMilliseconds / 1000) % 60; // Use modulo operator
      const elapsedMinutes = Math.floor(elapsedSeconds / 60) % 60;
      const elapsedHours = Math.floor(elapsedMinutes / 60) % 24;
      const elapsedDays = Math.floor(elapsedHours / 24);
      const elapsedTimeString = getElapsedTimeString(elapsedDays, elapsedHours, elapsedMinutes, elapsedSeconds);
      const currentDateTimeString = getCurrentDateTimeString();
      
      await publicRequest.put(`/documents/${id}`, { currentHolder: user});
      setDocument((prevDocument) => ({
        ...prevDocument,
        currentHolder: user
      }));
  
      const logs = await publicRequest.post(`/documents/${id}/logs`, {
        logTitle: `Document Arrived at ${userLocation} ${new Date().toLocaleString()}`,
        logDescription: `Accepted by: ${name} ${lastname}. Time Elapsed ${elapsedTimeString}.`,
        docId: userId
      });
      toast.success('Document Marked as Received Successfully.');
  
      console.log(logs);

          setDocument((prevDocument) => ({
      ...prevDocument,
      currentHolder: userId
    }));
  
      setLogTitle(`Document arrived on ${new Date().toLocaleDateString()}`);
      setLogDescription(user);
      setLogId(userId);
    } catch (error) {
      console.error(error);
      toast.error('Failed to update the document status.');
    }
  };
  
  const getElapsedTimeString = (days, hours, minutes, seconds) => {
    let elapsedTimeString = '';
    if (days > 0) {
      elapsedTimeString += `${days} day${days > 1 ? 's' : ''} `;
    }
    if (hours > 0) {
      elapsedTimeString += `${hours} hour${hours > 1 ? 's' : ''} `;
    }
    if (minutes > 0) {
      elapsedTimeString += `${minutes} minute${minutes > 1 ? 's' : ''} `;
    }
    if (seconds > 0) {
      elapsedTimeString += `${seconds} second${seconds > 1 ? 's' : ''}`;
    }
    return elapsedTimeString.trim();
  };
  
  
  



  
  const replies = document.replies || [];



  return (
    <div style={{ height: "100vh" }}>
      <TopBar />
      <div className="container">
        <div className="card" style={{ marginLeft: "46px", marginRight: "46px", marginTop: "20px" }}>

                  <div className="card-header bg-dark text-light">
            <div className="row align-items-center">
              <h5 className="card-title">{document.title}</h5>
              <p className='card-subtitle'> {document.docType} </p>
              <p className='card-title'> Destination : {document.location} </p>
              <p className='card-subtitle'> {document.transType} </p>
              <h6 className="card-title text-muted">{document.trackingNo}</h6>
              <p className="card-subtitle">Sender: {document.createdBy}</p>
              <p className="card-subtitle">Subject: {document.description}</p>
              <p className = "card-subtitle">To:  {document.recipient}</p>
               <div className="ml-auto">
               <button
                    onClick={handleLocation}
                    hidden={
                      document.status === 'completed' ||
                      
                      (document.recipient !== user && replies.length > 0 && !replies[replies.length - 1].replyRecipient) ||
                      (replies.length > 0 && replies[replies.length - 1].replyRecipient && replies[replies.length - 1].replyRecipient !== user)  && !(isAdmin || userRole !== 'standard')
                    }
                    style={{
                      position: 'absolute',
                      top: '10px',
                      right: '10px',
                      display: 'flex',
                      alignItems: 'center',
                      backgroundColor: '#c5d6c4',
                      border: 'none',
                      borderRadius: '5px',
                      padding: '0.5rem',
                      cursor: 'pointer',
                      boxShadow: '0px 2px 4px rgba(0, 0, 0, 0.2)',
                    }}
                  >                                
                    <FaEnvelope style={{ fontSize: '1.5rem', marginRight: '0.5rem' }} />
                    <span style={{ fontSize: '1rem' }}>Document Received</span>
                  </button>
              </div>
            </div>
          </div>

          <div className="card-body" style={{ minHeight: "25vh", margin: "20px" }}>
            <p className="card-text">{document.message}</p>
          </div>

          <div className="card-footer d-flex justify-content-end bg-dark">
                        <button
                className="btn btn-primary btn-sm mr-2"
                onClick={handleOpenReplyModal}
                disabled={
                  (document.transType === 'One Way' || document.status === 'completed' || document.currentHolder !== user) &&
                  !(isAdmin || userRole !== 'standard')
                }
                
              >
                <BsReply /> Transfer
              </button>
                  
        <button className="btn btn-success btn-sm mx-3" onClick={handleDone}
          disabled= {
            (document.currentHolder !== user || document.currentHolder !== user) && !(isAdmin || userRole !=='standard')
          }
        >
          <BsCheck /> Done
        </button>
        <button className="btn btn-warning btn-sm" onClick={handleFollowUp}
          disabled = {
            ( document.status === 'completed' || document.currentHolder === user ) && !(isAdmin || userRole !== 'standard')
           }
        > 
          <BsClock /> Follow up
        </button>
      </div>
        </div>



        <div className="footerDoc">

        {replies.map((reply, index) => (
        <div className="card" style={{ marginLeft: "46px", marginRight: "46px", marginTop: "20px" }} key={index}>
          <div className="card-header bg-dark text-light">
            <div className="row align-items-center">
              <h5 className="card-subtitle">Subject: {reply.subject}</h5>
              <p className='card-subtitle'>From: {user}</p>
              <p className='card-subtitle'>Destination: {reply.replyLocation}</p>
              <p className='card-subtitle'> To: {reply.replyRecipient}</p>
            </div>
          </div>
          <div className="card-body" style={{ minHeight: "5vh", margin: "20px" }}>
            <p className="card-text">{reply.reply}</p>
          </div>
        </div>
      ))}


        <div className="card bg-light" style={{ marginLeft: "100px", marginRight: "100px", marginTop: "40px", marginBottom:"10px" }}>
          <ul className="list-group list-group-flush">
            {document.logs && document.logs.map((log, index) => (
              <li className="list-group-item" key={index}>
                <h6 className="card-subtitle mb-2 text-muted">{log.logTitle}</h6>
                <p className="card-text">{log.logDescription}</p>
              </li>
            ))}
          </ul>
        </div>
      </div>
      </div>
      <ToastContainer />

      <Modal show={showReplyModal} onHide={handleCloseReplyModal}>
        <Modal.Body>
          <ReplyModal />
        </Modal.Body>
        <Modal.Footer>
          {/* <button className="btn btn-secondary" onClick={handleCloseReplyModal}>
            Close
          </button>
          <button className="btn btn-primary">
            Reply
          </button> */}
        </Modal.Footer>
      </Modal>
    </div>
  );
}

export default DocumentContent;
