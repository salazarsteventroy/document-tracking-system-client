// documentReducer.js
import {
    SEND_DOCUMENT_REQUEST,
    SEND_DOCUMENT_SUCCESS,
    SEND_DOCUMENT_FAILURE
  } from './actionTypes';
  
  const initialState = {
    loading: false,
    success: false,
    error: null
  };
  
  const documentReducer = (state = initialState, action) => {
    switch (action.type) {
      case SEND_DOCUMENT_REQUEST:
        return { ...state, loading: true, success: false, error: null };
      case SEND_DOCUMENT_SUCCESS:
        return { ...state, loading: false, success: true };
      case SEND_DOCUMENT_FAILURE:
        return { ...state, loading: false, error: action.payload };
      default:
        return state;
    }
  };
  
  export default documentReducer;
  