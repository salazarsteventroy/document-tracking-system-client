import { loginStart, loginSuccess,loginFailure, registerStart, registerSuccess, registerFailed} from "./userRedux";
import { publicRequest } from "../requestMethods";
import axios from 'axios';




export const login = async (dispatch, user) => {
    dispatch(loginStart());

    try {
        const res = await publicRequest.post("/auth/login", user);
        dispatch(loginSuccess(res.data));
    }catch (err) {
        dispatch(loginFailure());
    }
};

export const register = (newUser) => {
    return async (dispatch) => {
      try {
        // Dispatch an action to indicate the registration process has started
        dispatch({ type: 'registerStart' });
  
        // Make the API call to register the new user
        const response = await axios.post('http://localhost:5000/api/auth/register', newUser);
  
        // Dispatch an action with the received data if registration is successful
        dispatch({ type: 'registerSuccess', payload: response.data });
      } catch (error) {
        // Dispatch an action with the error if registration fails
        dispatch({ type: 'registerFailure', payload: error.message });
      }
    };
  };
  
