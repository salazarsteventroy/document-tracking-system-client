// documentActions.js
import axios from 'axios';
import {
  SEND_DOCUMENT_REQUEST,
  SEND_DOCUMENT_SUCCESS,
  SEND_DOCUMENT_FAILURE
} from './actionTypes';

export const sendDocument = (documentData) => {
  return async (dispatch) => {
    dispatch({ type: SEND_DOCUMENT_REQUEST });

    try {
      const response = await axios.post('http://localhost:5000/api/documents', documentData);
      dispatch({ type: SEND_DOCUMENT_SUCCESS, payload: response.data });
    } catch (error) {
      dispatch({ type: SEND_DOCUMENT_FAILURE, payload: error.message });
    }
  };
};
